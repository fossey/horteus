![Horteus Logo](https://gitlab.ecole.ensicaen.fr/intensif2018gr4/HorteUs/uploads/e01fcc45a153f0ada8b3962dcd27f342/label_horteus.png)

## Sujet : **Cultiver en milieu urbain collectif et partagé**.

### Notre projet : Horte'Us.

Le but du projet est de fournir une application connectée et collective permettant la gestion simple et intuitive d'un potager connecté.

Les **3 axes principaux** du projet sont :

    - faciliter le lien social entre les personnes à l'aide d'un projet commun
    - exploiter les données de capteurs pour aider les utilisateurs à entretenir leur espace
    - la mise à disposition d'une base de connaissance pour faciliter l'exploitation du jardin
    
La finalité du projet est de **faciliter** la création de jardins collectifs et d'**inciter** des personnes à s'en servir.

### Pourquoi Horte'Us

Le projet est né suite à un constat simple. La société urbaine actuelle n'est pas propice à la création de liens sociaux.
Les jardins collectifs étant de plus en plus populaires, ***Horte'Us*** est la pour en faciliter l'exploitation et la création.

### Comment ?

***Horte'Us*** est divisé en 3 modules :

    - une association capteur(s) <=> Raspberry Pi pour la récupération de données
    - un Web Service, central au projet
    - une interface web, pour l'affichage, la gestion et la mise en forme des données et des jardins
    
A la création d'un jardin collectif, un projet ***Horte'Us*** est créé.


### Technologies utilisées

#### Hardware

Les éléments hardware sont les suivants :

    - raspberry pi 3
    - sonde humidité en sol
    - sonde humidité en hors sol & température
    - routeur

#### Software

##### Communication Sonde - Serveur

MQTT est utilisé pour la communication Sonde - Serveur. Un ***broker*** publique (iot.eclipse.org) est utilisé pour le dépot d'informations côté raspberry
et pour "subscribe" côté webservice.

A chaque fois qu'une donnée est publiée par la raspberry, le serveur qui a "subscribe" au "topic" correspondant les reçoit.

Elles sont de la forme :

    id;air_humidity;temperature;ground_humidity

##### Back End - WebService

Le WebService est un serveur SpringBoot. La persistance des données est assurée grâçe à un noeud Elasticsearch qui est capable de support un de très gros volume de donnée.

##### Front End - Interface Web

L'interface Web a été construite à l'aide de Bootstrap, pour l'apparence et le côté responsive qu'il propose et de JQuery / Ajax pour l'exploitation et 
l'affichage des données.

### Administration technique :

* Lancement du serveur horteus :

```shell
> mvn spring-boot:run
```

* Lancement du serveur horteus avec purge de la base :

```shell
> mvn clean spring-boot:run
```

### Code de la sonde Horte'Us pour Arduino :
* ![Fichier .ino](https://gitlab.ecole.ensicaen.fr/intensif2018gr4/HorteUs/blob/develop/docs/horteurs_sensor.ino)


### Documentation :
API documentation : _/horteus/swagger-ui.html_

### Resources

Trello : https://trello.com/equipe4projetintensif2018