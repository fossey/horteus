package fr.ensicaen.horteus.api.documentation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Date;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket getSwaggerApi() {
        // @formatter:off
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(doBuildApiInfo())
                .useDefaultResponseMessages(false)
                .directModelSubstitute(Date.class, String.class)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
        // @formatter:on
    }

    private static ApiInfo doBuildApiInfo() {
        // @formatter:off
        return new ApiInfoBuilder().title("Horte'Us API")
                .description("Horte'Us\n\n Git : https://gitlab.ecole.ensicaen.fr/intensif2018gr4/HorteUs \n\n"+
                        "Trello : https://trello.com/equipe4projetintensif2018\n\n")
                .contact(new Contact("Romain FOSSEY - ENSICAEN / 3A. Info. App. (2018)\n",
                        "https://gitlab.ecole.ensicaen.fr/users/fossey/projects",
                        "fossey@ecole.ensicaen.fr"))
                .version("1.0")
                .build();
        // @formatter:on
    }

}
