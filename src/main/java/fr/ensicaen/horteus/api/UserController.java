package fr.ensicaen.horteus.api;

import fr.ensicaen.horteus.api.error.AlreadyExistException;
import fr.ensicaen.horteus.api.error.BadAuthentificationException;
import fr.ensicaen.horteus.domain.UserService;
import fr.ensicaen.horteus.model.User;
import fr.ensicaen.horteus.model.request.UserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "/users")
public class UserController {

    @Autowired
    private UserService service;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> get(Pageable pageable) {
        return service.getAll(pageable).getContent();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public User addUser(@RequestBody User user) throws AlreadyExistException {
        return service.save(user);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public User getById(@PathVariable("id") String id) {
        return service.getUser(id);
    }

    @RequestMapping(path = "/check", consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public User check(@RequestBody UserRequest request) throws BadAuthentificationException {
        return service.checkPassword(request);
    }

}
