package fr.ensicaen.horteus.api;

import fr.ensicaen.horteus.api.error.HorteusException;
import fr.ensicaen.horteus.domain.GardenService;
import fr.ensicaen.horteus.model.Garden;
import fr.ensicaen.horteus.model.Plant;
import fr.ensicaen.horteus.model.SensorBroker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping(path = "/gardens")
public class GardenController {

    @Autowired
    private GardenService gardenService;

    @RequestMapping(path = "/",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Garden> getAll(Pageable pageable) {
        return gardenService.getAll(pageable).getContent();
    }

    @RequestMapping(path = "/{gardenId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Garden getGarden(@PathVariable("gardenId") String gardenId) {
        return gardenService.getGarden(gardenId);
    }

    @RequestMapping(path = "/{gardenId}/sensors/brokers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void registerSensorBroker(@PathVariable("gardenId") String gardenId, @RequestBody SensorBroker sensorBroker) throws HorteusException {
        gardenService.registerSensorBroker(gardenId, sensorBroker);
    }

    @RequestMapping(path = "/{gardenId}/sensors/brokers/reset", method = RequestMethod.GET)
    public void resetSensorBrokerConnections(@PathVariable("gardenId") String gardenId) {
        gardenService.resetSensorBrokerConnections(gardenId);
    }

    @RequestMapping(path = "/{gardenId}/sensors", method = RequestMethod.GET)
    public Map<String, List<String>> getSensors(@PathVariable("gardenId") String gardenId) {
        return gardenService.getSensors(gardenId);
    }

    @RequestMapping(path = "/{gardenId}/plants", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Plant addPlant(@PathVariable("gardenId") String gardenId, @RequestBody Plant plant) {
        return gardenService.addPlant(gardenId, plant);
    }

    @RequestMapping(path = "/{gardenId}/plants", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Plant> getPlants(@PathVariable("gardenId") String gardenId) {
        return gardenService.getPlants(gardenId);
    }

}