package fr.ensicaen.horteus.api;

import fr.ensicaen.horteus.api.error.AlreadyExistException;
import fr.ensicaen.horteus.domain.ProjectService;
import fr.ensicaen.horteus.model.Garden;
import fr.ensicaen.horteus.model.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "/projects")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Project> getAll(Pageable pageable) {
        return projectService.getAll(pageable).getContent();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Project createProject(@RequestBody Project project) throws AlreadyExistException {
        return projectService.save(project);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Project getProject(@PathVariable("id") String id) {
        return projectService.getProject(id);
    }

    @RequestMapping(path = "/{id}/gardens", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Garden addGarden(@PathVariable("id") String projectId, @RequestBody Garden garden) {
        return projectService.addGarden(projectId, garden);
    }

    @RequestMapping(path = "/{id}/gardens", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Garden> getGardens(@PathVariable("id") String projectId) {
        return projectService.getGardens(projectId);
    }

}
