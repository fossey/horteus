package fr.ensicaen.horteus.api.error;

public class HorteusException extends Exception {

   public HorteusException(String s){
       super(s);
   }

    public HorteusException(Throwable e){
        super(e);
    }

}
