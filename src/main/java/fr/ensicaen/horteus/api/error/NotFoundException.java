package fr.ensicaen.horteus.api.error;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String s){
        super(s);
    }
}
