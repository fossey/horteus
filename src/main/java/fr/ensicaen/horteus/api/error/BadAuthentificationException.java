package fr.ensicaen.horteus.api.error;

public class BadAuthentificationException extends Exception {

    public BadAuthentificationException(String s){
        super(s);
    }

}
