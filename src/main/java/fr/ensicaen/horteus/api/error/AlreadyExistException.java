package fr.ensicaen.horteus.api.error;

public class AlreadyExistException extends HorteusException {

    public AlreadyExistException(String s){
        super(s);
    }

}

