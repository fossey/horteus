package fr.ensicaen.horteus.domain;

import fr.ensicaen.horteus.api.error.AlreadyExistException;
import fr.ensicaen.horteus.api.error.BadAuthentificationException;
import fr.ensicaen.horteus.api.error.NotFoundException;
import fr.ensicaen.horteus.dao.repository.UserRepository;
import fr.ensicaen.horteus.model.User;
import fr.ensicaen.horteus.model.request.UserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public User save(User user) throws AlreadyExistException {

        UserRequest request = new UserRequest();
        request.setAlias(user.getAlias());
        request.setPassword(user.getPassword());
        request.setEmail(user.getEmail());

        try {
            this.exist(request);
        } catch (NotFoundException e) {
            return repository.save(user);
        }

        throw new AlreadyExistException("User already exist");
    }

    public User getUser(String id) {

        User u = repository.findOne(id);

        if (u == null) {
            throw new NotFoundException("User not found");
        }

        return u;
    }

    public Page<User> getAll(Pageable p) {
        return repository.findAll(p);
    }

    public User exist(UserRequest request) {

        List<User> users = repository.findByEmail(request.getEmail());

        if (users == null || users.isEmpty()) {
            throw new NotFoundException("User not found");
        }

        return users.get(0);
    }

    public User checkPassword(UserRequest request) throws BadAuthentificationException {

        User u = exist(request);

        if (request.getPassword().equals(u.getPassword())) {
            return u;
        }

        throw new BadAuthentificationException("Bad password");
    }

}