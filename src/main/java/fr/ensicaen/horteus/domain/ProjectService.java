package fr.ensicaen.horteus.domain;

import fr.ensicaen.horteus.api.error.AlreadyExistException;
import fr.ensicaen.horteus.api.error.NotFoundException;
import fr.ensicaen.horteus.dao.repository.ProjectRepository;
import fr.ensicaen.horteus.model.Garden;
import fr.ensicaen.horteus.model.Project;
import fr.ensicaen.horteus.model.request.ProjectRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectService {

    @Autowired
    private ProjectRepository repository;

    @Autowired
    private GardenService gardenService;

    public Project save(Project project) throws AlreadyExistException {

        List<Project> projects = repository.findByName(project.getName());

        if (!projects.isEmpty()) {
            throw new AlreadyExistException("Project " + project.getName() + " already exist.");
        }

        return repository.save(project);
    }

    public Project getProject(String id) {

        Project p = repository.findOne(id);

        if (p == null) {
            throw new NotFoundException("Project not found");
        }

        return p;
    }

    public Page<Project> getAll(Pageable p) {
        return repository.findAll(p);
    }

    public Project exist(ProjectRequest request) {

        List<Project> projects = repository.findByName(request.getName());

        if (projects == null || projects.isEmpty()) {
            throw new NotFoundException("Project not found");
        }
        return projects.get(0);
    }

    public Garden addGarden(String projectId, Garden garden) {

        Project project = getProject(projectId);
        String adminId = project.getAdminId();
        garden.getUsersId().add(adminId);

        Garden saved = gardenService.save(garden);
        project = getProject(projectId);
        project.getGardensId().add(saved.getId());

        repository.save(project); //Update project

        return saved;
    }

    public List<Garden> getGardens(String projectId) {

        Project project = getProject(projectId);

        List<Garden> gardens = new ArrayList<>();
        project.getGardensId().forEach(gardenId -> gardens.add(gardenService.getGarden(gardenId)));

        return gardens;
    }
}