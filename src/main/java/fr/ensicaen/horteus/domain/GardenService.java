package fr.ensicaen.horteus.domain;

import fr.ensicaen.horteus.api.error.HorteusException;
import fr.ensicaen.horteus.api.error.NotFoundException;
import fr.ensicaen.horteus.dao.SubscriberDao;
import fr.ensicaen.horteus.dao.repository.GardenRepository;
import fr.ensicaen.horteus.model.Garden;
import fr.ensicaen.horteus.model.Plant;
import fr.ensicaen.horteus.model.SensorBroker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class GardenService {

    @Autowired
    public GardenRepository repository;

    private Logger logger = LoggerFactory.getLogger(GardenService.class);

    public Page<Garden> getAll(Pageable pageable){
        return repository.findAll(pageable);
    }

    public Garden save(Garden garden) {
        return repository.save(garden);
    }

    public Garden getGarden(String id) {

        Garden g = repository.findOne(id);

        if (g == null) {
            throw new NotFoundException("Garden not found : " + id);
        }
        return g;
    }

    public List<Garden> getGardenByName(String name) {
        return repository.findByName(name);
    }

    public Plant addPlant(String gardenId, Plant plant) {

        Garden garden = getGarden(gardenId);

        garden.getPlants().add(plant);
        save(garden);

        return plant;
    }

    public void postData(String gardenId, Map<String, String> values) {

        Garden garden = getGarden(gardenId);

        values.forEach(
                (name, value) -> {
                    garden.getSensors().get(name).add(value);
                    logger.info("Sensor {} - {}", name, value);
                }
        );

        repository.save(garden);
    }

    public void resetSensorBrokerConnections(String gardenId) {
        getGarden(gardenId)
                .getSensorBrokers()
                .stream().forEach(
                sensorBroker -> {

                    try {
                        SubscriberDao.getInstance().startSubscriber(sensorBroker, gardenId, this);
                    } catch (HorteusException e) {
                        logger.error("Impossible to restart broker : {}", sensorBroker.getBroker());
                    }
                }
        );
    }

    public void addSensor(String gardenId, String sensorName) {
        Garden garden = getGarden(gardenId);
        garden.getSensors().put(sensorName, new ArrayList<>());
        save(garden);
        logger.info("Sensor {} added", sensorName);
    }

    public void registerSensorBroker(String gardenId, SensorBroker sensorBroker) throws HorteusException {
        Garden garden = getGarden(gardenId);
        garden.getSensorBrokers().add(sensorBroker);
        save(garden);

        addSensor(gardenId, "air_humidity");
        addSensor(gardenId, "ground_humidity");
        addSensor(gardenId, "temperature");

        SubscriberDao.getInstance().startSubscriber(sensorBroker, gardenId, this);

    }

    public Map<String, List<String>> getSensors(String gardenId) {
        return getGarden(gardenId).getSensors();
    }

    public List<Plant> getPlants(String gardenId) {
        return getGarden(gardenId).getPlants();
    }
}
