package fr.ensicaen.horteus.domain.mqtt;

import fr.ensicaen.horteus.api.error.HorteusException;
import fr.ensicaen.horteus.domain.GardenService;
import fr.ensicaen.horteus.model.SensorBroker;
import org.eclipse.paho.client.mqttv3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Subscriber implements MqttCallback {


    private Logger logger = LoggerFactory.getLogger(Subscriber.class);
    private MqttClient client;
    private String gardenId;
    private GardenService gardenService;
    private IMqttMessageParser iParser;

    public Subscriber(SensorBroker brokerInfo, String gardenId, IMqttMessageParser iParser, GardenService service) throws HorteusException {

        this.gardenId = gardenId;
        this.iParser = iParser;
        this.gardenService = service;

        try {

            this.client = new MqttClient(brokerInfo.getBroker(), brokerInfo.getClientName());
            this.client.connect();
            this.client.setCallback(this);
            this.client.subscribe(brokerInfo.getTopic());

        } catch (MqttException me) {
            logger.error("SensorBroker subscribing failed - Topic: {} Broker: {} ClientName: {}",
                    brokerInfo.getTopic(), brokerInfo.getBroker(), brokerInfo.getClientName());
            logger.error(me.getMessage());
            throw new HorteusException(me);
        }

        logger.info("Broker {} connected", brokerInfo.getBroker());
    }

    @Override
    public void connectionLost(Throwable e) {
        logger.error("Connection lost : {} {}", e.getMessage(), e.getCause().getMessage());
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        logger.trace("deliveryComplete - token {}", token);
    }

    @Override
    public void messageArrived(String topic, MqttMessage msg) {
        logger.debug("MqttMessage {}", msg);
        gardenService.postData(gardenId, iParser.parseMqttMessage(msg));
    }
}