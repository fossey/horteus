package fr.ensicaen.horteus.domain.mqtt;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.Map;

public interface IMqttMessageParser {

    Map<String, String> parseMqttMessage(MqttMessage message);

}