package fr.ensicaen.horteus.domain.mqtt;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.HashMap;
import java.util.Map;

public class MqttMessageParser implements IMqttMessageParser {

    @Override
    public Map<String, String> parseMqttMessage(MqttMessage mqttMessage) {

        String[] msg = mqttMessage.toString().split(";");

        Map<String, String> values = new HashMap<>();

        values.put("air_humidity", msg[1]);
        values.put("temperature", msg[2]);
        values.put("ground_humidity", msg[3]);

        return values;
    }
}
