package fr.ensicaen.horteus.model;

import lombok.Getter;
import lombok.Setter;

public class Plant {

    @Getter @Setter
    private String name;
    
    @Getter @Setter
    private String information;

}

