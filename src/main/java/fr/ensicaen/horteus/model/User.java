package fr.ensicaen.horteus.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(type = "user", indexName = "horteus")
public class User{

    @Id
    @Getter @Setter
    private String id;

    @Getter @Setter
    private String lastname;

    @Getter @Setter
    private String firstname;

    @Getter @Setter
    private String alias;

    @Getter @Setter
    private String password;
    
    @Getter @Setter
    private String email;
}
