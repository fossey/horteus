package fr.ensicaen.horteus.model.request;

import lombok.Getter;
import lombok.Setter;

public class UserRequest {

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String password;
    
    @Getter
    @Setter
    private String alias;

}
