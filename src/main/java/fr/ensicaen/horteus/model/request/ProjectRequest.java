package fr.ensicaen.horteus.model.request;

import fr.ensicaen.horteus.model.User;
import lombok.Getter;
import lombok.Setter;

public class ProjectRequest {
	
	@Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private User admin;
    
    @Getter
    @Setter
    private String location;

}
