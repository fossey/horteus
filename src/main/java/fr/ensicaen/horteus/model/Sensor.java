package fr.ensicaen.horteus.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class Sensor {

    public Sensor(){
        this.name = "Default Sensor";
    }

    public Sensor(String name){
        this.name = name;
    }

    @Getter @Setter
    private String name;

    @Getter
    private List<String> values = new ArrayList<>();

}
