package fr.ensicaen.horteus.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(type = "project", indexName = "horteus")
public class Project {

    @Id
    @Getter @Setter
    private String id;

    @Getter @Setter
    private String adminId;

    @Getter
    private List<String> gardensId = new ArrayList<>();
    
    @Getter @Setter
    private String name;

    @Getter @Setter
    private String description;

    @Getter @Setter
    private String location;
    
    @Getter @Setter
    private String locationLatitude;

    @Getter @Setter
    private String locationLongitude;
    
    @Getter @Setter
    private Date creationDate;

}

