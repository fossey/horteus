package fr.ensicaen.horteus.model;

import lombok.Getter;
import lombok.Setter;

public class SensorBroker {

    @Getter @Setter
    private String topic;

    @Getter @Setter
    private String broker;

    @Getter @Setter
    private String clientName;

}
