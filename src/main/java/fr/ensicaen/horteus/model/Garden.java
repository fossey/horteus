package fr.ensicaen.horteus.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Document(type = "garden", indexName = "horteus")
public class Garden {

    @Id
    @Getter @Setter
    private String id;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private String description;

    @Getter
    private List<Plant> plants = new ArrayList<>();

    @Getter
    private List<String> usersId = new ArrayList<>();

    @Getter
    private HashMap<String , List<String>> sensors = new HashMap<>();

    @Getter
    private List<SensorBroker> sensorBrokers = new ArrayList<>();

}
