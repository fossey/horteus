package fr.ensicaen.horteus.dao.repository;

import fr.ensicaen.horteus.model.Garden;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface GardenRepository extends ElasticsearchRepository<Garden, String> {

    List<Garden> findByName(String name);
}
