package fr.ensicaen.horteus.dao.repository;

import fr.ensicaen.horteus.model.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface UserRepository extends ElasticsearchRepository<User, String> {

    List<User> findByEmail(String email);

}
