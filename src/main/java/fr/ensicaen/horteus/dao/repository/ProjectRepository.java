package fr.ensicaen.horteus.dao.repository;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import fr.ensicaen.horteus.model.Project;

public interface ProjectRepository extends ElasticsearchRepository<Project, String> {
	
	List<Project> findByName(String name);

}
