package fr.ensicaen.horteus.dao;

import fr.ensicaen.horteus.api.error.HorteusException;
import fr.ensicaen.horteus.domain.GardenService;
import fr.ensicaen.horteus.domain.mqtt.MqttMessageParser;
import fr.ensicaen.horteus.domain.mqtt.Subscriber;
import fr.ensicaen.horteus.model.SensorBroker;

public class SubscriberDao {

    private static SubscriberDao subscriberDao;

    private SubscriberDao() {
    }

    public static SubscriberDao getInstance() {

        if (subscriberDao == null) {
            subscriberDao = new SubscriberDao();
        }

        return subscriberDao;
    }

    public void startSubscriber(SensorBroker sensorBroker, String gardenId, GardenService service) throws HorteusException {
        new Subscriber(sensorBroker, gardenId, new MqttMessageParser(), service);
    }
}
