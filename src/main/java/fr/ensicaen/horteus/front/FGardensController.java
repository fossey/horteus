package fr.ensicaen.horteus.front;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FGardensController {

    @RequestMapping("/mygardens")
    public String dispatch(@RequestParam(value = "id", required = false, defaultValue = "unknown") String id, Model model) {
        model.addAttribute("id", id);
        return "mygardens";
    }
}
