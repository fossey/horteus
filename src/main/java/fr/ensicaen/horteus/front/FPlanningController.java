package fr.ensicaen.horteus.front;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FPlanningController {

    @RequestMapping("/myplanning")
    public String dispatch() {
        return "myplanning";
    }

}
