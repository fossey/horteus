package fr.ensicaen.horteus.front;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FHomeController {

    @RequestMapping("/home")
    public String dispatch() {
        return "home";
    }

}
