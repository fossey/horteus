package fr.ensicaen.horteus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HorteusApplication {

    public static void main(String[] args) {
        SpringApplication.run(HorteusApplication.class, args);
    }

}
