var webservice = {

    host: "dome4.ensicaen.fr",
    port: 8080,
    name: "horteus",

    getUrl: function() {
        return this.host + ":" + this.port + "/" + this.name;
    },

    getHttpUrl: function () {
        return "http://" + this.getUrl();
    },

    getProjects: function (callback) {
        $.ajax({
            type: "GET",
            url: this.getHttpUrl() + "/projects",
            dataType: "json",
            error: function (error) {
                console.log(error);
            }
        }).done(callback);
    },

    getGarden: function (id, callback) {
        $.ajax({
            type: "GET",
            url: this.getHttpUrl() + "/gardens/" + id,
            dataType: "json",
            error: function (error) {
                console.log(error);
            }
        }).done(callback);
    },

    getGardens: function (id, callback) {
        $.ajax({
            type: "GET",
            url: this.getHttpUrl() + "/projects/"+id+"/gardens",
            dataType: "json",
            error: function (error) {
                console.log(error);
            }
        }).done(callback);
    },

    getSensors: function (gardenId , callback) {
        $.ajax({
            type: "GET",
            url: this.getHttpUrl() + "/gardens/" + gardenId + "/sensors",
            dataType: "json",
            error: function (error) {
                console.log(error);
            }
        }).done(callback);
    }
};

