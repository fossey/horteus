package fr.ensicaen.horteus.util;

import fr.ensicaen.horteus.model.Garden;
import fr.ensicaen.horteus.model.Plant;
import fr.ensicaen.horteus.model.Project;
import fr.ensicaen.horteus.model.User;

import java.util.Date;

public class DummyFactory {

    public static Project createProject() {

        Project project = new Project();
        project.setName("Horteus");
        project.setAdminId("userId");
        project.setCreationDate(new Date());

        return project;
    }

    public static User createUser() {

        User u = new User();
        u.setFirstname("jean");
        u.setLastname("dupond");
        u.setEmail("first.last@test.com");
        u.setPassword("a1b2c3d4e5");

        return u;
    }

    public static User createUserWithSpecificEmail(String s) {

        User u = createUser();
        u.setEmail(s);

        return u;
    }

    public static Garden createGarden() {

        Garden g = new Garden();
        g.setName("MyGarden");

        return g;
    }

    public static Plant createPlant() {

        Plant p = new Plant();
        p.setName("Tomato");
        p.setInformation("Tomato - Description");

        return p;
    }
}
