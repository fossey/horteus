package fr.ensicaen.horteus.domain;

import fr.ensicaen.horteus.api.error.AlreadyExistException;
import fr.ensicaen.horteus.dao.repository.GardenRepository;
import fr.ensicaen.horteus.dao.repository.ProjectRepository;
import fr.ensicaen.horteus.dao.repository.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import fr.ensicaen.horteus.HorteusApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = HorteusApplication.class)
public abstract class AbstractServiceTest {

    @Autowired
    protected UserRepository userRepository;

    @Autowired
    protected ProjectRepository projectRepository;

    @Autowired
    protected GardenRepository gardenRepository;

    @Before
    public void setUp() throws AlreadyExistException {
        this.userRepository.deleteAll();
        this.gardenRepository.deleteAll();
        this.projectRepository.deleteAll();
    }

    @After
    public void downUp() {
        this.userRepository.deleteAll();
        this.gardenRepository.deleteAll();
        this.projectRepository.deleteAll();
    }

}
