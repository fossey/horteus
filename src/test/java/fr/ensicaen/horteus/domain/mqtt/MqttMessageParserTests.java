package fr.ensicaen.horteus.domain.mqtt;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MqttMessageParserTests {

    IMqttMessageParser parser = new MqttMessageParser();

    @Test
    public void testParser(){

        MqttMessage msg = new MqttMessage();
        msg.setPayload("90;34.60;22.20;11".getBytes());

        Map<String, String> results = parser.parseMqttMessage(msg);

        assertEquals(3,results.size());
        assertEquals("34.60",results.get("air_humidity"));
        assertEquals("22.20",results.get("temperature"));
        assertEquals("11",results.get("ground_humidity"));

    }

}
