package fr.ensicaen.horteus.domain;

import fr.ensicaen.horteus.api.error.AlreadyExistException;
import fr.ensicaen.horteus.api.error.NotFoundException;
import fr.ensicaen.horteus.model.Garden;
import fr.ensicaen.horteus.model.Plant;
import fr.ensicaen.horteus.model.Project;
import fr.ensicaen.horteus.util.DummyFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProjectServiceTests extends AbstractServiceTest {

    @Autowired
    ProjectService projectService;

    @Test
    public void testSaveProject() throws AlreadyExistException {

        Project project = DummyFactory.createProject();

        Project tested = projectService.save(project);

        assertNotNull(tested.getId());
        assertNotNull(tested.getGardensId());

    }


    @Test
    public void testAddGarden() throws AlreadyExistException, NotFoundException {

        Project project = projectService.save(DummyFactory.createProject());

        Garden garden = DummyFactory.createGarden();

        Garden gardenStored = projectService.addGarden(project.getId(), garden);

        assertNotNull(gardenStored.getId());
        assertEquals(1, garden.getUsersId().size());
        assertEquals(1, projectService.getProject(project.getId()).getGardensId().size());

    }

}