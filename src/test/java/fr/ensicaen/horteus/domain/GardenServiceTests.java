package fr.ensicaen.horteus.domain;


import fr.ensicaen.horteus.api.error.AlreadyExistException;
import fr.ensicaen.horteus.api.error.NotFoundException;
import fr.ensicaen.horteus.model.Garden;
import fr.ensicaen.horteus.model.Plant;
import fr.ensicaen.horteus.model.Project;
import fr.ensicaen.horteus.model.Sensor;
import fr.ensicaen.horteus.util.DummyFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class GardenServiceTests extends AbstractServiceTest {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private GardenService gardenService;

    @Test(expected = NotFoundException.class)
    public void getNonexistentId() {

        this.gardenService.getGarden("fakeID12");

        fail("NotFoundException not throw");

    }

    @Test
    public void testAddPlant() throws AlreadyExistException, NotFoundException {

        Project project = projectService.save(DummyFactory.createProject());
        Garden garden = projectService.addGarden(project.getId(), DummyFactory.createGarden());

        Plant plant = DummyFactory.createPlant();

        Plant plantStored = gardenService.addPlant(garden.getId(), plant);

        assertEquals(plant.getName(), plantStored.getName());
        assertEquals(plant.getInformation(), plantStored.getInformation());

        assertEquals(1, gardenService.getGarden(garden.getId()).getPlants().size());

    }

    @Test
    public void testPostData() throws AlreadyExistException {

        Project project = projectService.save(DummyFactory.createProject());
        Garden garden = projectService.addGarden(project.getId(), DummyFactory.createGarden());

        gardenService.addSensor(garden.getId(), "air_humidity");
        gardenService.addSensor(garden.getId(), "ground_humidity");
        gardenService.addSensor(garden.getId(), "temperature");

        Map<String, String> values = new HashMap<>();
        values.put("air_humidity", "10.22");
        values.put("ground_humidity", "60.22");
        values.put("temperature", "15");

        gardenService.postData(garden.getId(), values);

        Garden gardenTested = gardenService.getGarden(garden.getId());

        assertEquals(3, gardenTested.getSensors().size());

        assertTrue(gardenTested.getSensors().get("air_humidity").contains("10.22"));
        assertTrue(gardenTested.getSensors().get("ground_humidity").contains("60.22"));
        assertTrue(gardenTested.getSensors().get("temperature").contains("15"));

    }
}