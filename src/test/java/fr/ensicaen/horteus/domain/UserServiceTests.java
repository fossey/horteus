package fr.ensicaen.horteus.domain;

import fr.ensicaen.horteus.api.error.AlreadyExistException;
import fr.ensicaen.horteus.api.error.BadAuthentificationException;
import fr.ensicaen.horteus.api.error.NotFoundException;
import fr.ensicaen.horteus.model.User;
import fr.ensicaen.horteus.model.request.UserRequest;
import fr.ensicaen.horteus.util.DummyFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class UserServiceTests extends AbstractServiceTest{

    @Autowired
    private UserService service;

    @Test
    public void addUser() throws Exception {

        User u = DummyFactory.createUser();

        User uAdded = this.service.save(u);

        assertNotNull(uAdded.getId());
        assertEquals("jean",uAdded.getFirstname());
        assertEquals("dupond",uAdded.getLastname());
        assertEquals("first.last@test.com",uAdded.getEmail());
        assertEquals("a1b2c3d4e5",uAdded.getPassword());

    }

    @Test(expected = AlreadyExistException.class)
    public void addExistingUser() throws Exception {

        User u = DummyFactory.createUser();

        this.service.save(u);
        this.service.save(u);

        fail("Exception not throw");
    }

    @Test
    public void getAllUsers() throws Exception {

        this.service.save(DummyFactory.createUserWithSpecificEmail("user1@test.com"));
        this.service.save(DummyFactory.createUserWithSpecificEmail("user2@test.com"));
        this.service.save(DummyFactory.createUserWithSpecificEmail("user3@test.com"));

        List<User> list = this.service.getAll(new PageRequest(0,10)).getContent();

        assertEquals(3,list.size());

    }

    @Test
    public void getById() throws Exception {

        User u = this.userRepository.save(DummyFactory.createUser());

        User uAdded = this.service.getUser(u.getId());

        assertNotNull(uAdded.getId());
        assertEquals("jean",uAdded.getFirstname());
        assertEquals("dupond",uAdded.getLastname());
        assertEquals("first.last@test.com",uAdded.getEmail());
        assertEquals("a1b2c3d4e5",uAdded.getPassword());

    }


    @Test(expected = NotFoundException.class)
    public void getNonexistentId() throws Exception {

        this.service.getUser("fakeID12");

        fail("NotFoundException not throw");

    }

    @Test
    public void checkCorrectPassword() throws Exception {

        User u = this.userRepository.save(DummyFactory.createUser());

        UserRequest request = new UserRequest();
        request.setEmail(u.getEmail());
        request.setPassword(u.getPassword());

        User userRetrieved = this.service.checkPassword(request);

        assertEquals(u.getId(),userRetrieved.getId());

    }

    @Test(expected = BadAuthentificationException.class)
    public void checkBadPassword() throws Exception {

        User u = this.userRepository.save(DummyFactory.createUser());

        UserRequest request = new UserRequest();
        request.setEmail(u.getEmail());
        request.setPassword("BadPassword!");

        this.service.checkPassword(request);

        fail("BadAuthenticationException not throw");
    }


    @Test(expected = NotFoundException.class)
    public void checkPasswordForNonexistentUser() throws Exception {

        UserRequest request = new UserRequest();
        request.setEmail("fake.user@test.com");

        this.service.checkPassword(request);

        fail("NotFoundException not throw");
    }
}