#include <string.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "DHTesp.h"
#define soilMoisterPin A0
#define soilMoisterVcc D4
int soilMoister = 0;

DHTesp dht;

const char* ssid = "DOME_FORMATION";
const char* password = "dome_formation";
const char* mqtt_server = "dome4.ensicaen.fr";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup() {
  Serial.println("debut void setup");
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  Serial.println("lacer callback");
  client.setCallback(callback);
  Serial.println("je suis sorti callback");

  Serial.println();
  Serial.println("Status\tHumidity (%)\tTemperature (C)\t(F)\tHeatIndex (C)\t(F)");
  digitalWrite (soilMoisterVcc, LOW);
  dht.setup(D1); // data pin 2
  
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }

  Serial.println();
  Serial.println("je callback");

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

  Serial.println("on arrive a la fin du callback");

}


/***************************************************
 * Get Soil Moister Sensor data
 **************************************************/
int getSoilMoisterData(void)
{
  soilMoister = 0;
  digitalWrite (soilMoisterVcc, HIGH);
  delay (500);
  int N = 3;
  for(int i = 0; i < N; i++) // read sensor "N" times and get the average
  {
    soilMoister += analogRead(soilMoisterPin);
    Serial.print(soilMoister);   
    delay(150);
  }
  digitalWrite (soilMoisterVcc, LOW); 
  Serial.println(soilMoister);
  return soilMoister;
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.subscribe("inTopic");
      Serial.println("suscribed");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {

  if (!client.connected()) {
    Serial.println("je rentre dans le if");
    reconnect();
  }

  client.loop();

  long now = millis();
  if (now - lastMsg > 30000) {
    lastMsg = now;
    delay(dht.getMinimumSamplingPeriod());

    float humidity = dht.getHumidity();
    float temperature = dht.getTemperature();

    char chrmsg[50];
    String strmsg = String(value) + ";" + String(humidity) + ";" + String(temperature)+ ";" + String(getSoilMoisterData());;
    strmsg.toCharArray(chrmsg,50);
    //delay (30000);

    ++value;
    
    Serial.print("Publish message: ");
    Serial.println(chrmsg);
    client.publish("HORTEUS_SENSORS", chrmsg);
  }

}